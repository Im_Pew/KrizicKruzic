package dturic.faculty.person.hr.krizickruzic.udp;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketTimeoutException;

import dturic.faculty.person.hr.krizickruzic.udp.model.PlayerMoveData;

public class GameUDPConnectionListen extends AsyncTask<PlayerMoveData, String, String> {

    private static DatagramSocket socket;
    private static DatagramPacket packet;
    private static OnGameMoveResponse sListener;

    public static void setGameMoveListener(OnGameMoveResponse listener) {
        sListener = listener;
    }

    public static void setSocket(DatagramSocket socket) {
        GameUDPConnectionListen.socket = socket;
    }

    @Override
    protected String doInBackground(PlayerMoveData... voids) {

        String modifiedSentence = "";
        byte[] receivedData = new byte[1024];
        packet = new DatagramPacket(receivedData, receivedData.length);
        try {
            //socket.setSoTimeout(10000);
            try {
                socket.receive(packet);
            } catch (SocketTimeoutException timeOut) {
                sListener.onTimeout();
                return "NOT_OK";
            }
            socket.setSoTimeout(0);

            modifiedSentence = new String(packet.getData(), 0, packet.getLength());

        } catch (Exception e) {
            e.printStackTrace();
        }
        return modifiedSentence;
    }

    @Override
    protected void onPostExecute(String response) {
        super.onPostExecute(response);

        if (!response.equals("")) {
            if (response.equals("OK")) {
                sListener.onGameConfirm();
            } else if (response.equals("NOT_OK")){
                sListener.onTimeout();
            } else {
                Gson gson = new Gson();
                PlayerMoveData move = gson.fromJson(response, PlayerMoveData.class);
                sListener.onReceivedAnswer(move);
            }
        }
    }


    public interface OnGameMoveResponse {
        void onReceivedAnswer(PlayerMoveData move);

        void onGameConfirm();

        void onTimeout();
    }
}
