package dturic.faculty.person.hr.krizickruzic.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.fragment.MainScreenLoggedFragment;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class LoginDialogFragment extends DialogFragment implements View.OnClickListener, DataProvider.LoginListener {

    private TextView mUsername;
    private TextView mPassword;

    public static LoginDialogFragment newInstance() {
        return new LoginDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_login_layout, container);

        mUsername = v.findViewById(R.id.username);
        mPassword = v.findViewById(R.id.password);

        v.findViewById(R.id.login).setOnClickListener(this);
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        DataProvider.getInstance().setLoginListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        DataProvider.getInstance().setLoginListener(null);
    }

    @Override
    public void onClick(View v) {
        DataProvider.getInstance().login(mUsername.getText().toString(), mPassword.getText().toString());
    }

    @Override
    public void onSuccess(LoginData data) {
        mUsername.setText(data.getEmail());

        UserUtil.saveUser(getContext(), data);
        getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content, MainScreenLoggedFragment.newInstance()).commit();
    }

    @Override
    public void onFailure(String message) {
        mPassword.setText(message);
    }
}
