package dturic.faculty.person.hr.krizickruzic.http.models;

import com.google.gson.annotations.SerializedName;

public class StatisticsDataResponse {

    @SerializedName("code")
    private int code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private StatisticsResonse data;

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public StatisticsResonse getData() {
        return data;
    }
}

