package dturic.faculty.person.hr.krizickruzic.udp;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import dturic.faculty.person.hr.krizickruzic.udp.model.PlayerMoveData;

public class GameUDPConnection extends AsyncTask<PlayerMoveData, String, String> {

    private static DatagramSocket socket;
    private static DatagramPacket packet;

    static private byte[] sByteAddress;

    public static void setServerAddress(byte[] addr) {
        sByteAddress = addr;
    }

    public static void closeSocket() {
        socket.close();
        socket = null;
    }

    @Override
    protected String doInBackground(PlayerMoveData... voids) {

        String modifiedSentence = "";

        try {
            InetAddress address = InetAddress.getByAddress(sByteAddress);


            if (socket == null || socket.isClosed()) {
                socket = new DatagramSocket();
                GameUDPConnectionListen.setSocket(socket);
                new GameUDPConnectionListen().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            String playerMoveJson = new Gson().toJson(voids[0]);

            packet = new DatagramPacket(playerMoveJson.getBytes(), playerMoveJson.length(), address, 4000);
            socket.send(packet);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return modifiedSentence;
    }
}