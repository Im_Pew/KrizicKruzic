package dturic.faculty.person.hr.krizickruzic.udp;

import android.os.Handler;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import dturic.faculty.person.hr.krizickruzic.udp.model.FriendGameRequest;
import dturic.faculty.person.hr.krizickruzic.udp.model.GameData;

public class RematchListener extends Thread {

    private String mUserId;
    private String mStatus;
    private GameData mGameData;
    private DatagramSocket socket;
    private InetAddress mAddress;
    private static OnRematch sListener;
    private Handler handler;

    public RematchListener(String address, OnRematch listener, GameData data, String status, String userId) {
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        try {
            mAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        handler = new Handler();
        sListener = listener;
        mGameData = data;
        mUserId = userId;
        mStatus = status;
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    @Override
    public void run() {
        DatagramPacket packet;

        FriendGameRequest response = null;
        byte[] receivedData = new byte[1024];
        try {

            Gson gson = new Gson();
            FriendGameRequest request = new FriendGameRequest();

            request.setGameData(mGameData);

            request.setUserId(mUserId);
            request.setFriendUserId(mUserId);
            request.setStatus(mStatus);

            String jsonData = gson.toJson(request);

            packet = new DatagramPacket(jsonData.getBytes(), jsonData.length(), mAddress, 1500);

            socket.send(packet);

            packet = new DatagramPacket(receivedData, receivedData.length);

            socket.receive(packet);

            response = gson.fromJson(new String(packet.getData(), 0, packet.getLength()), FriendGameRequest.class);


        } catch (Exception e) {
            e.printStackTrace();
        }

        if (response != null) {
            final FriendGameRequest finalResponse = response;
            if (response.getStatus().equals("Accepted")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sListener.onConfirm(finalResponse.getGameData());
                    }
                });
            } else if (response.getStatus().equals("Denied")) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        sListener.onDecline("Denied");
                    }
                });
            }
        }
    }

    public interface OnRematch {
        void onConfirm(GameData game);

        void onDecline(String status);
    }

}
