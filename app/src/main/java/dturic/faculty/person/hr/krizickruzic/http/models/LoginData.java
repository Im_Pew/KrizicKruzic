package dturic.faculty.person.hr.krizickruzic.http.models;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class LoginData implements Serializable {

    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("email")
    private String email;
    @SerializedName("user_id")
    private String userId;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getUserId() {
        return userId;
    }
}
