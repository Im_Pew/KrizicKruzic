package dturic.faculty.person.hr.krizickruzic.http;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public interface RetrofitProvider {
    void login(String username, String password);

    void register(String username, String email, String password);

    void forgottenPassword(String username, String email);

    void getInQuery(String userId);

    void resetPassword(String password, String repeatedPassword, String email, String newPassword);

    void getFriendList(String userId);

    void getUserById(String userId);

    void getServerAddress();

    void acceptFriendRequest(String userId, String friendId);

    void declineFriendRequest(String userId, String friendId);

    void removePlayerFromFriendQueue(String userId);

    void sendFriendRequest(String userId, String username, String friendUsername);

    void getFriendRequests(String userId);

    void getStatistics(String userId);

}
