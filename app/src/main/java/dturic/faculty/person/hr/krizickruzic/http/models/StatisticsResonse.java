package dturic.faculty.person.hr.krizickruzic.http.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class StatisticsResonse {
    private int wins;
    private int draws;
    private int loses;
    @SerializedName("games")
    private ArrayList<GameResponse> lastFiveGames;

    public int getWins() {
        return wins;
    }

    public int getDraws() {
        return draws;
    }

    public int getLoses() {
        return loses;
    }

    public ArrayList<GameResponse> getLastFiveGames() {
        return lastFiveGames;
    }
}
