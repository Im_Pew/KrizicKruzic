package dturic.faculty.person.hr.krizickruzic.udp;


import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import dturic.faculty.person.hr.krizickruzic.udp.model.PlayerMoveData;

public class GameUDPConnectionThread extends Thread {

    private static DatagramSocket socket;
    private static DatagramPacket packet;
    private static byte[] sByteAddress;
    private static PlayerMoveData mMove;

    public synchronized void start(PlayerMoveData move) {
        mMove = move;
        super.start();
    }

    @Override
    public void run() {
        super.run();

        try {
            InetAddress address = InetAddress.getByAddress(sByteAddress);


            if (socket == null || socket.isClosed()) {
                socket = new DatagramSocket();
                GameUDPConnectionListen.setSocket(socket);
                new GameUDPConnectionListen().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            }

            String playerMoveJson = new Gson().toJson(mMove);
            Log.i("Move JSON data: ", playerMoveJson);

            packet = new DatagramPacket(playerMoveJson.getBytes(), playerMoveJson.length(), address, 4000);
            socket.send(packet);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setServerAddress(byte[] addr) {
        sByteAddress = addr;
    }

    public static void closeSocket() {
        socket.close();
        socket = null;
    }

}
