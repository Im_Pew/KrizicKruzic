package dturic.faculty.person.hr.krizickruzic.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.DatagramSocketImpl;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketOptions;
import java.net.UnknownHostException;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.fragment.dialog.FriendListDialogFragment;
import dturic.faculty.person.hr.krizickruzic.fragment.dialog.ProfileDialogFragment;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;
import dturic.faculty.person.hr.krizickruzic.http.models.Friend;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;
import dturic.faculty.person.hr.krizickruzic.service.FriendGameListener;
import dturic.faculty.person.hr.krizickruzic.udp.GameUDPConnection;
import dturic.faculty.person.hr.krizickruzic.udp.GameUDPConnectionThread;
import dturic.faculty.person.hr.krizickruzic.udp.SendFriendGameRequest;
import dturic.faculty.person.hr.krizickruzic.udp.UDPConnection;
import dturic.faculty.person.hr.krizickruzic.udp.model.FriendGameRequest;

public class MainScreenLoggedFragment extends Fragment implements DataProvider.QueueListener, FriendListDialogFragment.OnItemSelected, FriendGameListener.OnRequest {

    private Button mFindGame;
    private Button mFiendGame;
    private ImageButton mProfile;

    private Friend mFriend;
    private LoginData mUser;

    public static MainScreenLoggedFragment newInstance() {
        return new MainScreenLoggedFragment();
    }

    public static MainScreenLoggedFragment newInstance(boolean again) {
        MainScreenLoggedFragment f = new MainScreenLoggedFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("game", again);
        f.setArguments(bundle);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onPause() {
        super.onPause();

        stopFriendService();
    }

    @Override
    public void onResume() {
        super.onResume();

        getActivity().startService(new Intent(getContext(), FriendGameListener.class));
        mUser = UserUtil.getUser(getContext());
        FriendGameListener.setOnRequestListener(this);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_screen_logged, container, false);

        mFindGame = view.findViewById(R.id.find_game);
        mFiendGame = view.findViewById(R.id.friend_game);
        mProfile = view.findViewById(R.id.user_profile);

        final FriendListDialogFragment f = FriendListDialogFragment.newInstance();

        f.setOnSelectListener(this);
        mFiendGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                f.show(getChildFragmentManager(), "");
            }
        });

        mProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileDialogFragment profileDialogFragment = ProfileDialogFragment.newInstance();
                profileDialogFragment.show(getChildFragmentManager(), "profile_fragment");
            }
        });

        Bundle bundle = getArguments();
        if (bundle == null) {
            mFindGame.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FragmentActivity activity = getActivity();
                    stopFriendService();

                    mFindGame.setText(R.string.searching);
                    mFindGame.setEnabled(false);
                    mUser = UserUtil.getUser(getContext());
                    DataProvider.getInstance().getInQuery(mUser.getUserId());
                    DataProvider.getInstance().setQueueListener(MainScreenLoggedFragment.this);
                }
            });
        } else {
            mUser = UserUtil.getUser(getContext());
            DataProvider.getInstance().getInQuery(mUser.getUserId());
            mFindGame.setText(R.string.searching);
            mFindGame.setEnabled(false);

            FragmentActivity activity = getActivity();
            stopFriendService();

        }

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.profile, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.user_profile) {
            ProfileDialogFragment profileDialogFragment = ProfileDialogFragment.newInstance();
            profileDialogFragment.show(getChildFragmentManager(), "profile_fragment");
            return false;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccessQueue(final String address) {
        Log.i("Address", address);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    UDPConnection.setServerAddress(InetAddress.getByName(address).getAddress());
                    //GameUDPConnection.setServerAddress(InetAddress.getByName(address).getAddress());
                    GameUDPConnectionThread.setServerAddress(InetAddress.getByName(address).getAddress());
                    getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content, OnGameReadyFragment.newInstance(), "game_fragment").commit();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    @Override
    public void onFailureQueue(String message) {

    }

    @Override
    public void onSelect(Friend friend) {
        mFriend = friend;
        String address = UserUtil.getServerAddress(getContext());
        new SendFriendGameRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, address, mUser.getUserId(), mFriend.getUserId(), "Ask");
    }

    private void showFriendGameRequest(final FriendGameRequest data) {
        final String jsonData = new Gson().toJson(data.getGameData());
        AlertDialog dialog = new AlertDialog.Builder(getContext()).setTitle(R.string.friend_game).setMessage(R.string.friend_game_message).setNegativeButton("Deny", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //TODO GameAcceptAsyncTask

                String address = UserUtil.getServerAddress(getContext());
                new SendFriendGameRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, address, data.getUserId(), data.getFriendUserId(), "Deny", jsonData);

            }
        }).setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                String address = UserUtil.getServerAddress(getContext());
                new SendFriendGameRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, address, data.getUserId(), data.getFriendUserId(), "Accept", jsonData);
            }
        }).create();

        dialog.show();
    }

    private void stopFriendService() {
        new Thread(new Runnable() {

            @Override
            public void run() {

                DatagramSocket socket;
                DatagramPacket packet;

                try {
                    socket = new DatagramSocket();
                } catch (SocketException e) {
                    e.printStackTrace();
                    return;
                }

                Gson gson = new Gson();
                FriendGameRequest request = new FriendGameRequest();
                request.setUserId(mUser.getUserId());
                request.setStatus("Unregister");
                String jsonData = gson.toJson(request);

                try {
                    packet = new DatagramPacket(jsonData.getBytes(), jsonData.length(), InetAddress.getByName(UserUtil.getServerAddress(getContext())), 1500);
                    socket.send(packet);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

        getActivity().stopService(new Intent(getContext(), FriendGameListener.class));
    }

    @Override
    public void onRequest(FriendGameRequest data) {
        showFriendGameRequest(data);
    }
}
