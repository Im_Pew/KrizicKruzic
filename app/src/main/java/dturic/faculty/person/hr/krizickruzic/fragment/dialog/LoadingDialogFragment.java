package dturic.faculty.person.hr.krizickruzic.fragment.dialog;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import dturic.faculty.person.hr.krizickruzic.R;

public class LoadingDialogFragment extends DialogFragment {

    ImageView mLoadingIcon;
    TextView mLoadingMessage;

    public static LoadingDialogFragment newInstance() {
        return new LoadingDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.loading_dialog_fragment, container, false);

        mLoadingIcon = view.findViewById(R.id.loading_icon);
        mLoadingMessage = view.findViewById(R.id.loading_message);

        mLoadingMessage.setText(R.string.loading);

        mLoadingIcon.setBackgroundResource(R.drawable.loading);
        AnimationDrawable animation = (AnimationDrawable) mLoadingIcon.getBackground();

        animation.start();
        return view;
    }
}
