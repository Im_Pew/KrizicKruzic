package dturic.faculty.person.hr.krizickruzic.http.models;

import com.google.gson.annotations.SerializedName;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class Friend {

    @SerializedName("user_id")
    private String userId;
    @SerializedName("username")
    private String username;

    public String getUserId() {
        return userId;
    }

    public String getUsername() {
        return username;
    }
}
