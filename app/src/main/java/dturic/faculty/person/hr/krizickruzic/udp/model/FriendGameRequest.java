package dturic.faculty.person.hr.krizickruzic.udp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class FriendGameRequest implements Serializable {

    @SerializedName("user_id")
    private String userId;
    @SerializedName("friend_user_id")
    private String friendUserId;
    @SerializedName("status")
    private String status;
    @SerializedName("game_data")
    private GameData gameData;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFriendUserId() {
        return friendUserId;
    }

    public void setFriendUserId(String friendUserId) {
        this.friendUserId = friendUserId;
    }

    public GameData getGameData() {
        return gameData;
    }

    public void setGameData(GameData gameData) {
        this.gameData = gameData;
    }
}
