package dturic.faculty.person.hr.krizickruzic.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.http.models.Friend;

public class RequestsFriendListAdapter extends RecyclerView.Adapter<RequestsFriendListAdapter.ViewHolder> {

    private ArrayList<Friend> mData;
    private OnFriendRequestAction mListener;

    @NonNull
    @Override
    public RequestsFriendListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_list_request_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mUsername.setText(mData.get(position).getUsername());
    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        } else {
            return 0;
        }
    }

    public void setOnFriendRequestActionListener(OnFriendRequestAction listener) {
        mListener = listener;
    }

    public void setData(ArrayList<Friend> data) {
        mData = data;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mUsername;
        private Button mAccept;
        private Button mDecline;

        ViewHolder(View itemView) {
            super(itemView);

            mUsername = itemView.findViewById(R.id.player_username);

            mAccept = itemView.findViewById(R.id.accept);
            mDecline = itemView.findViewById(R.id.decline);

            mAccept.setOnClickListener(this);
            mDecline.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.accept:
                    mListener.onAccept(mData.get(getAdapterPosition()));
                    break;
                case R.id.decline:
                    mListener.onDecline(mData.get(getAdapterPosition()));
                    break;
            }
            mData.remove(getAdapterPosition());
            notifyDataSetChanged();
        }
    }

    public interface OnFriendRequestAction {
        void onAccept(Friend friend);

        void onDecline(Friend friend);
    }
}
