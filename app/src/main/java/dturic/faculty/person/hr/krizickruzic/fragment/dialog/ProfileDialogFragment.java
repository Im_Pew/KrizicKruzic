package dturic.faculty.person.hr.krizickruzic.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.fragment.MainScreenFragment;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;
import dturic.faculty.person.hr.krizickruzic.http.models.Friend;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;

public class ProfileDialogFragment extends DialogFragment implements View.OnClickListener, FriendListDialogFragment.OnItemSelected {

    private TextView mUsername;
    private TextView mEmail;
    private Button mLogout;
    private Button mFriendRequests;
    private Button mStatistics;

    static public ProfileDialogFragment newInstance() {
        return new ProfileDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_profile, container);

        mUsername = view.findViewById(R.id.username);
        mEmail = view.findViewById(R.id.email);
        mLogout = view.findViewById(R.id.log_out);
        mFriendRequests = view.findViewById(R.id.friend_requests);
        mStatistics = view.findViewById(R.id.statistics);
        mLogout.setOnClickListener(this);
        mFriendRequests.setOnClickListener(this);
        mStatistics.setOnClickListener(this);

        setText();
        return view;
    }

    private void setText() {
        LoginData user = UserUtil.getUser(getContext());

        mUsername.setText(user.getUsername());
        mEmail.setText(user.getEmail());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.log_out:
                UserUtil.saveUser(getContext(), null);
                dismiss();

                getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content, MainScreenFragment.newInstance()).commit();

                break;
            case R.id.friend_requests:
                FriendListDialogFragment f = FriendListDialogFragment.newInstance("requests");
                f.setOnSelectListener(ProfileDialogFragment.this);
                f.show(getChildFragmentManager(), "friends_requests");
                break;
            case R.id.statistics:
                StatisticsDialogFragment fra = StatisticsDialogFragment.newInstance();
                fra.show(getChildFragmentManager(), "statistics");
                break;
        }
    }

    @Override
    public void onSelect(Friend friend) {
        //TODO accept request
    }
}
