package dturic.faculty.person.hr.krizickruzic;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import java.net.InetAddress;

import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class UserUtil {

    private final static String USER_DATA = "shared_preferences_data";
    private final static String SERVER_ADDRESS = "shared_preferences_server_address";


    public static void saveUser(Context context, LoginData data) {
        SharedPreferences preferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);

        Gson gson = new Gson();

        preferences.edit().putString("user", gson.toJson(data)).apply();
    }

    public static LoginData getUser(Context context) {

        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        String user = sharedPreferences.getString("user", null);
        if (user == null) {
            return null;
        }

        Gson gson = new Gson();
        return gson.fromJson(user, LoginData.class);
    }

    public static String getServerAddress(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);
        String address = sharedPreferences.getString("address", "");
        if (address.equals("")) {
            return "";
        }

        return address;
    }

    public static void saveServerAddress(Context context, String address) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(USER_DATA, Context.MODE_PRIVATE);

        sharedPreferences.edit().putString("address", address).apply();
    }
}
