package dturic.faculty.person.hr.krizickruzic.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.fragment.MainScreenLoggedFragment;
import dturic.faculty.person.hr.krizickruzic.fragment.OnGameReadyFragment;
import dturic.faculty.person.hr.krizickruzic.udp.GameUDPConnection;
import dturic.faculty.person.hr.krizickruzic.udp.RematchListener;
import dturic.faculty.person.hr.krizickruzic.udp.model.GameData;

public class EndScreenDialogFragment extends DialogFragment implements View.OnClickListener, RematchListener.OnRematch {

    private TextView mTittle;
    private TextView mMessage;
    private Button mDecline;
    private Button mAccept;
    private GameData gameData;

    public static EndScreenDialogFragment newInstance(int win, GameData data) {
        EndScreenDialogFragment f = new EndScreenDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putSerializable("game", data);
        bundle.putInt("win", win);
        f.setArguments(bundle);

        return f;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.end_screen_dialog_fragment, container);

        int win = 0;
        if (getArguments() != null) {
            win = getArguments().getInt("win");
            gameData = (GameData) getArguments().getSerializable("game");
        }

        mTittle = view.findViewById(R.id.tittle);
        mMessage = view.findViewById(R.id.message);
        mDecline = view.findViewById(R.id.decline);
        mAccept = view.findViewById(R.id.accept);

        setupText(win);
        mDecline.setOnClickListener(this);
        mAccept.setOnClickListener(this);

        return view;
    }

    private void setupText(int win) {
        if (win == 1) {
            mTittle.setText(R.string.won_tittle);
            mMessage.setText(R.string.won_description);
        } else if (win == 2) {
            mTittle.setText(R.string.lose_tittle);
            mMessage.setText(R.string.lose_description);
        } else if (win == 3) {
            mTittle.setText(R.string.draw_tittle);
            mMessage.setText(R.string.draw_descripetion);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.decline:
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .replace(android.R.id.content, MainScreenLoggedFragment.newInstance())
                        .commit();
                break;
            case R.id.accept:
                //TODO get data of last game and collect needed details
                String serverAddress = UserUtil.getServerAddress(getContext());
                String userId = UserUtil.getUser(getContext()).getUserId();
                new RematchListener(serverAddress, this, gameData, "Again", userId).start();
                //new SendFriendGameRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, serverAddress, UserUtil.getUser(getContext()).getUserId(), null, "Again", new Gson().toJson(gameData));
                break;
        }
    }

    @Override
    public void onConfirm(GameData game) {
        Toast.makeText(getContext(), "Dobiod sam pozitivan odgovor", Toast.LENGTH_SHORT).show();
        getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content, OnGameReadyFragment.newInstance(game)).commit();
    }

    @Override
    public void onDecline(String status) {
        Toast.makeText(getContext(), "NO NO", Toast.LENGTH_SHORT).show();
    }
}
