package dturic.faculty.person.hr.krizickruzic.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.fragment.MainScreenFragment;
import dturic.faculty.person.hr.krizickruzic.fragment.MainScreenLoggedFragment;
import dturic.faculty.person.hr.krizickruzic.fragment.OnGameReadyFragment;
import dturic.faculty.person.hr.krizickruzic.udp.SendFriendGameRequest;
import dturic.faculty.person.hr.krizickruzic.udp.model.GameData;

public class MainActivity extends AppCompatActivity implements SendFriendGameRequest.OnGameConfirm {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SendFriendGameRequest.setListener(this);

        if (UserUtil.getUser(this) != null) {
            getSupportFragmentManager().beginTransaction().replace(android.R.id.content, MainScreenLoggedFragment.newInstance(), "anonymous").commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(android.R.id.content, MainScreenFragment.newInstance(), "logged").commit();
        }
    }


    @Override
    public void onConfirm(GameData game) {
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, OnGameReadyFragment.newInstance(game), "game").commit();
    }

    @Override
    public void onDecline(String status) {
        getSupportFragmentManager().beginTransaction().replace(android.R.id.content, MainScreenLoggedFragment.newInstance(), "GAME").commit();
    }
}