package dturic.faculty.person.hr.krizickruzic.http.models;

import java.util.ArrayList;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class ResponseFriends {

    private String code;
    private String message;
    private ArrayList<Friend> data;


    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public ArrayList<Friend> getData() {
        return data;
    }
}
