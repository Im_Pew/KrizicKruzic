package dturic.faculty.person.hr.krizickruzic.fragment;

import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.fragment.dialog.EndScreenDialogFragment;
import dturic.faculty.person.hr.krizickruzic.fragment.dialog.LoadingDialogFragment;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;
import dturic.faculty.person.hr.krizickruzic.udp.GameListener;
import dturic.faculty.person.hr.krizickruzic.udp.GameUDPConnection;
import dturic.faculty.person.hr.krizickruzic.udp.GameUDPConnectionListen;
import dturic.faculty.person.hr.krizickruzic.udp.GameUDPConnectionThread;
import dturic.faculty.person.hr.krizickruzic.udp.UDPConnection;
import dturic.faculty.person.hr.krizickruzic.udp.model.GameData;
import dturic.faculty.person.hr.krizickruzic.udp.model.PlayerMoveData;

public class OnGameReadyFragment extends Fragment implements View.OnClickListener, GameListener.OnGameReady, GameUDPConnectionListen.OnGameMoveResponse {

    private static final String GAME_DATA_BUNDLE = "game_data";
    private static final String END_SCREEN_FRAGMENT_TAG = "end_screen_dialog_fragment";

    private ImageButton mFieldOne;
    private ImageButton mFieldTwo;
    private ImageButton mFieldThree;
    private ImageButton mFieldFour;
    private ImageButton mFieldFive;
    private ImageButton mFieldSix;
    private ImageButton mFieldSeven;
    private ImageButton mFieldEight;
    private ImageButton mFieldNine;
    private TextView mGameInfo;

    private LoadingDialogFragment f;

    private LoginData mUser;
    private PlayerMoveData mMove;
    private GameData mGameData;

    public static OnGameReadyFragment newInstance() {
        return new OnGameReadyFragment();
    }

    public static OnGameReadyFragment newInstance(GameData gameData) {
        Bundle bundle = new Bundle();

        bundle.putSerializable("game_data", gameData);

        OnGameReadyFragment f = new OnGameReadyFragment();
        f.setArguments(bundle);

        return f;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game, container, false);

        init(view);

        f = LoadingDialogFragment.newInstance();
        f.show(getChildFragmentManager(), "loading");

        GameUDPConnectionListen.setGameMoveListener(this);

        mUser = UserUtil.getUser(Objects.requireNonNull(getContext()));

        mMove = new PlayerMoveData();
        mMove.setUserId(mUser.getUserId());

        if (mUser == null) {
            assert getActivity() != null;
            getActivity().finish();
        }

        if (getArguments() != null) {
            if (getArguments().getSerializable(GAME_DATA_BUNDLE) != null) {
                String address = UserUtil.getServerAddress(getContext());

                try {
                    //GameUDPConnection.setServerAddress(InetAddress.getByName(address).getAddress());
                    GameUDPConnectionThread.setServerAddress(InetAddress.getByName(address).getAddress());
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }

                onFound((GameData) getArguments().getSerializable(GAME_DATA_BUNDLE));

            }
        } else {
            GameListener.setListener(this);
            UDPConnection.setsUserId(mUser.getUserId());
            new UDPConnection().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "Register");
        }
        return view;
    }

    private void init(View v) {
        mFieldOne = v.findViewById(R.id.game_board_field_one);
        mFieldTwo = v.findViewById(R.id.game_board_field_two);
        mFieldThree = v.findViewById(R.id.game_board_field_three);
        mFieldFour = v.findViewById(R.id.game_board_field_four);
        mFieldFive = v.findViewById(R.id.game_board_field_five);
        mFieldSix = v.findViewById(R.id.game_board_field_six);
        mFieldSeven = v.findViewById(R.id.game_board_field_seven);
        mFieldEight = v.findViewById(R.id.game_board_field_eight);
        mFieldNine = v.findViewById(R.id.game_board_field_nine);
        mGameInfo = v.findViewById(R.id.game_info);
    }

    private void temporallyDisableButtons(boolean disable) {

        if (disable) {
            mGameInfo.setText(R.string.opponent);
            mFieldOne.setOnClickListener(null);
            mFieldTwo.setOnClickListener(null);
            mFieldThree.setOnClickListener(null);
            mFieldFour.setOnClickListener(null);
            mFieldFive.setOnClickListener(null);
            mFieldSix.setOnClickListener(null);
            mFieldSeven.setOnClickListener(null);
            mFieldEight.setOnClickListener(null);
            mFieldNine.setOnClickListener(null);
        } else {
            mGameInfo.setText(R.string.player);
            mFieldOne.setOnClickListener(this);
            mFieldTwo.setOnClickListener(this);
            mFieldThree.setOnClickListener(this);
            mFieldFour.setOnClickListener(this);
            mFieldFive.setOnClickListener(this);
            mFieldSix.setOnClickListener(this);
            mFieldSeven.setOnClickListener(this);
            mFieldEight.setOnClickListener(this);
            mFieldNine.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.game_board_field_one:
                disableField(1, mMove.getPlayer());
                mMove.setField(1);
                break;
            case R.id.game_board_field_two:
                disableField(2, mMove.getPlayer());
                mMove.setField(2);
                break;
            case R.id.game_board_field_three:
                disableField(3, mMove.getPlayer());
                mMove.setField(3);
                break;
            case R.id.game_board_field_four:
                disableField(4, mMove.getPlayer());
                mMove.setField(4);
                break;
            case R.id.game_board_field_five:
                disableField(5, mMove.getPlayer());
                mMove.setField(5);
                break;
            case R.id.game_board_field_six:
                disableField(6, mMove.getPlayer());
                mMove.setField(6);
                break;
            case R.id.game_board_field_seven:
                disableField(7, mMove.getPlayer());
                mMove.setField(7);
                break;
            case R.id.game_board_field_eight:
                disableField(8, mMove.getPlayer());
                mMove.setField(8);
                break;
            case R.id.game_board_field_nine:
                disableField(9, mMove.getPlayer());
                mMove.setField(9);
                break;
        }

        temporallyDisableButtons(true);
        //new GameUDPConnection().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mMove);
        new GameUDPConnectionThread().start(mMove);
        new GameUDPConnectionListen().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void disableField(int field, int player) {

        AnimationDrawable animation;
        int image;
        if (player == 1) {
            image = R.drawable.cross;
        } else {
            image = R.drawable.circle;
        }
        switch (field) {
            case 1:
                mFieldOne.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldOne.getBackground();
                animation.setOneShot(true);
                animation.start();

                mFieldOne.setClickable(false);
                break;
            case 2:
                mFieldTwo.setClickable(false);
                mFieldTwo.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldTwo.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
            case 3:
                mFieldThree.setClickable(false);
                mFieldThree.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldThree.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
            case 4:
                mFieldFour.setClickable(false);
                mFieldFour.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldFour.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
            case 5:
                mFieldFive.setClickable(false);
                mFieldFive.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldFive.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
            case 6:
                mFieldSix.setClickable(false);
                mFieldSix.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldSix.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
            case 7:
                mFieldSeven.setClickable(false);
                mFieldSeven.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldSeven.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
            case 8:
                mFieldEight.setClickable(false);
                mFieldEight.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldEight.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
            case 9:
                mFieldNine.setClickable(false);
                mFieldNine.setBackgroundResource(image);
                animation = (AnimationDrawable) mFieldNine.getBackground();
                animation.setOneShot(true);
                animation.start();
                break;
        }
    }

    @Override
    public void onFound(GameData game) {
        f.dismiss();
        mMove.setGameId(game.getGameId());

        mGameData = game;
        GameUDPConnectionListen.setGameMoveListener(this);
        if (mUser.getUserId().equals(game.getPlayerOne())) {
            mGameInfo.setText(R.string.player);
            mMove.setPlayer(1);
        } else {
            mGameInfo.setText(R.string.opponent);
            mMove.setPlayer(2);
        }
        mMove.setUserId(mUser.getUserId());
        new GameUDPConnectionThread().start(mMove);
        //new GameUDPConnection().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, mMove);
    }

    @Override
    public void onReceivedAnswer(PlayerMoveData data) {
        mMove.setTurn(data.getTurn() + 1);

        disableField(data.getField(), data.getPlayer());
        temporallyDisableButtons(false);

        if (data.getWinner() != 0) {

            if (data.getWinner() == mMove.getPlayer()) {
                Toast.makeText(getContext(), "You Win", Toast.LENGTH_SHORT).show();
                EndScreenDialogFragment.newInstance(1, mGameData).show(getChildFragmentManager(), END_SCREEN_FRAGMENT_TAG);
            } else if (data.getWinner() == 3) {
                Toast.makeText(getContext(), "Draw", Toast.LENGTH_SHORT).show();
                EndScreenDialogFragment.newInstance(3, mGameData).show(getChildFragmentManager(), END_SCREEN_FRAGMENT_TAG);
            } else {
                Toast.makeText(getContext(), "You lose", Toast.LENGTH_SHORT).show();
                EndScreenDialogFragment.newInstance(2, mGameData).show(getChildFragmentManager(), END_SCREEN_FRAGMENT_TAG);
            }

            GameUDPConnectionListen.setGameMoveListener(null);
            //GameUDPConnection.closeSocket();
            GameUDPConnectionThread.closeSocket();
        }
    }

    @Override
    public void onGameConfirm() {
        if (mMove.getPlayer() == 1) {
            mMove.setTurn(1);
            temporallyDisableButtons(false);
        } else {
            new GameUDPConnectionListen().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
    }

    @Override
    public void onTimeout() {
        getActivity().finish();
    }
}