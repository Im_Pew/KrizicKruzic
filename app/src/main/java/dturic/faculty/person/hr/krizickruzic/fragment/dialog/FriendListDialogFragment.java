package dturic.faculty.person.hr.krizickruzic.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.adapter.FriendListAdapter;
import dturic.faculty.person.hr.krizickruzic.adapter.RequestsFriendListAdapter;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;
import dturic.faculty.person.hr.krizickruzic.http.models.Friend;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;

public class FriendListDialogFragment extends DialogFragment implements FriendListAdapter.OnFriendSelected, DataProvider.FriendListListener, View.OnClickListener, RequestsFriendListAdapter.OnFriendRequestAction, DataProvider.FriendRequestListener {

    private RecyclerView mRecyclerView;
    private FriendListAdapter mAdapter;
    private RequestsFriendListAdapter mRequestListAdapter;
    private OnItemSelected mListener;
    private Button mAddFriend;
    private EditText mFriendUsername;
    private LoginData user;

    private String mMode;

    public void setOnSelectListener(OnItemSelected listener) {
        mListener = listener;
    }

    public static FriendListDialogFragment newInstance() {
        return new FriendListDialogFragment();
    }

    public static FriendListDialogFragment newInstance(String status) {

        FriendListDialogFragment f = new FriendListDialogFragment();

        Bundle bundle = new Bundle();
        bundle.putString("requests", status);

        f.setArguments(bundle);

        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        user = UserUtil.getUser(getContext());

        Bundle bundle = getArguments();
        if (bundle != null) {
            if (bundle.getString("requests") != null) {
                DataProvider.getInstance().getFriendRequests(user.getUserId());
                mRequestListAdapter = new RequestsFriendListAdapter();
                mRequestListAdapter.setOnFriendRequestActionListener(this);
                DataProvider.getInstance().setFriendRequestListener(this);
                mMode = "FriendRequests";
            }
        } else {
            DataProvider.getInstance().getFriendList(user.getUserId());
            mAdapter = new FriendListAdapter();
            mAdapter.setOnFriendSelectListener(this);
            mMode = "FriendGame";
        }

        DataProvider.getInstance().setFriendListListener(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_dialog_friend_list, container);

        mRecyclerView = view.findViewById(R.id.friend_list);
        mAddFriend = view.findViewById(R.id.add_friend);
        mFriendUsername = view.findViewById(R.id.friend_name_input);

        mAddFriend.setOnClickListener(this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        switch (mMode) {
            case "FriendGame":
                mRecyclerView.setAdapter(mAdapter);
                break;
            case "FriendRequests":
                mRecyclerView.setAdapter(mRequestListAdapter);
                break;
        }

        return view;
    }

    @Override
    public void onFriendSelected(Friend friend) {
        mListener.onSelect(friend);
        dismiss();
    }

    @Override
    public void onSucces(ArrayList<Friend> friends) {
        switch (mMode) {
            case "FriendGame":
                mAdapter.setData(friends);
                break;
            case "FriendRequests":
                mRequestListAdapter.setData(friends);
                break;
        }
    }

    @Override
    public void onFailure(String message) {

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.add_friend) {
            String friendUsername = mFriendUsername.getText().toString();
            if (!TextUtils.isEmpty(friendUsername)) {
                assert getContext() != null;
                LoginData user = UserUtil.getUser(getContext());
                assert user != null;
                DataProvider.getInstance().sendFriendRequest(user.getUserId(), user.getUsername(), friendUsername);
            } else {
                Toast.makeText(getContext(), R.string.enter_friend_username, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onAccept(Friend index) {
        DataProvider.getInstance().acceptFriendRequest(user.getUserId(), index.getUserId());
    }

    @Override
    public void onDecline(Friend index) {
        DataProvider.getInstance().declineFriendRequest(user.getUserId(), index.getUserId());
    }

    public interface OnItemSelected {
        void onSelect(Friend friend);
    }
}
