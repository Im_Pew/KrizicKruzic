package dturic.faculty.person.hr.krizickruzic.udp.model;

import com.google.gson.annotations.SerializedName;

public class GameRequest {

    @SerializedName("user_id")
    private String userId;
    @SerializedName("status")
    private String status;

    public GameRequest(String userId, String status) {
        this.userId = userId;
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
