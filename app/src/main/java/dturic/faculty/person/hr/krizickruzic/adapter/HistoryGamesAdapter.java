package dturic.faculty.person.hr.krizickruzic.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.http.models.GameResponse;

public class HistoryGamesAdapter extends RecyclerView.Adapter<HistoryGamesAdapter.ViewHolder> {
    private ArrayList<GameResponse> mData;

    @NonNull
    @Override
    public HistoryGamesAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.history_game_list_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull HistoryGamesAdapter.ViewHolder holder, int position) {

        Context context = holder.itemView.getContext();

        if (mData.get(position).isDraw()) {
            holder.mIndicator.setBackground(context.getDrawable(R.drawable.draw_indicator));
        } else if (mData.get(position).getWinner().equals(UserUtil.getUser(holder.itemView.getContext()).getUserId())) {
            holder.mIndicator.setBackground(context.getDrawable(R.drawable.win_indicator));
        } else {
            holder.mIndicator.setBackground(context.getDrawable(R.drawable.lose_indicator));
//            holder.mIndicator.setColorFilter(ContextCompat.getColor(context, android.R.color.holo_red_dark));
        }

        holder.mOpponent.setText(mData.get(position).getPlayerTwo());
        holder.mTurns.setText(String.valueOf(mData.get(position).getTurns()));
        holder.mTime.setText(new Date(mData.get(position).getTime()).toString());


    }

    @Override
    public int getItemCount() {
        return mData == null ? 0 : mData.size();
    }

    public void setData(ArrayList<GameResponse> lastFiveGames) {
        mData = lastFiveGames;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mIndicator;
        TextView mOpponent;
        TextView mTurns;
        TextView mTime;

        ViewHolder(View itemView) {
            super(itemView);

            mIndicator = itemView.findViewById(R.id.game_indicator);
            mOpponent = itemView.findViewById(R.id.opponent);
            mTurns = itemView.findViewById(R.id.turns);
            mTime = itemView.findViewById(R.id.time);
        }
    }
}
