package dturic.faculty.person.hr.krizickruzic.udp.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class GameData implements Serializable {

    @SerializedName("game_id")
    private String gameId;
    @SerializedName("player_one")
    private String playerOne;
    @SerializedName("player_two")
    private String playerTwo;

    public String getPlayerTwo() {
        return playerTwo;
    }

    public String getPlayerOne() {
        return playerOne;
    }

    public String getGameId() {
        return gameId;
    }
}
