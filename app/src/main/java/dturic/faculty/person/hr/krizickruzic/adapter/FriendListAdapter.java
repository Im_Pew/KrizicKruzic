package dturic.faculty.person.hr.krizickruzic.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.http.models.Friend;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {

    private ArrayList<Friend> mData;
    private OnFriendSelected mListener;

    @NonNull
    @Override
    public FriendListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.friend_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.mUsername.setText(mData.get(position).getUsername());
    }

    @Override
    public int getItemCount() {
        if (mData != null) {
            return mData.size();
        } else {
            return 0;
        }
    }

    public void setOnFriendSelectListener(OnFriendSelected listener) {
        mListener = listener;
    }

    public void setData(ArrayList<Friend> data) {
        mData = data;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mUsername;

        ViewHolder(View itemView) {
            super(itemView);

            mUsername = itemView.findViewById(R.id.player_username);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onFriendSelected(mData.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface OnFriendSelected {
        void onFriendSelected(Friend index);
    }
}
