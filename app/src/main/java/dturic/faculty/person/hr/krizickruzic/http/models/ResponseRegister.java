package dturic.faculty.person.hr.krizickruzic.http.models;

import com.google.gson.annotations.SerializedName;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class ResponseRegister {
    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
