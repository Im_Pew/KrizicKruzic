package dturic.faculty.person.hr.krizickruzic.udp;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import dturic.faculty.person.hr.krizickruzic.udp.model.FriendGameRequest;
import dturic.faculty.person.hr.krizickruzic.udp.model.GameData;

public class SendFriendGameRequest extends AsyncTask<String, FriendGameRequest, FriendGameRequest> {
    private DatagramSocket socket;
    private InetAddress mAddress;
    private static OnGameConfirm sListener;

    @Override
    protected FriendGameRequest doInBackground(String... voids) {
        DatagramPacket packet;
        try {
            socket = new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }

        if (voids.length > 5) {
            return null;
        } else {
            try {
                mAddress = InetAddress.getByName(voids[0]);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            }
        }
        FriendGameRequest response = null;
        byte[] receivedData = new byte[1024];
        try {

            Gson gson = new Gson();
            FriendGameRequest request = new FriendGameRequest();
            
            if (voids.length == 5) {
                GameData gameData = gson.fromJson(voids[4], GameData.class);
                request.setGameData(gameData);
            }

            request.setUserId(voids[1]);
            request.setFriendUserId(voids[2]);
            request.setStatus(voids[3]);

            String jsonData = gson.toJson(request);

            packet = new DatagramPacket(jsonData.getBytes(), jsonData.length(), mAddress, 1500);

            socket.send(packet);

            packet = new DatagramPacket(receivedData, receivedData.length);

            socket.receive(packet);

            response = gson.fromJson(new String(packet.getData(), 0, packet.getLength()), FriendGameRequest.class);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @Override
    protected void onPostExecute(FriendGameRequest response) {
        super.onPostExecute(response);

        if (response != null) {
            if (response.getStatus().equals("Not Found")) {
                sListener.onDecline(response.getStatus());
            } else if (response.getStatus().equals("Accept")) {
                sListener.onConfirm(response.getGameData());
            } else if (response.getStatus().equals("Deny")) {
                sListener.onDecline(response.getStatus());
            }
        }
    }

    public static void setListener(OnGameConfirm mListener) {
        sListener = mListener;
    }

    public interface OnGameConfirm {
        void onConfirm(GameData game);

        void onDecline(String status);
    }
}
