package dturic.faculty.person.hr.krizickruzic.udp;

import android.os.AsyncTask;

public class ConnectionUpdater extends Thread {

    private static boolean sInQueue;

    public static void setInQueue(boolean status) {
        sInQueue = status;
    }


    @Override
    public void run() {
        super.run();

        try {
            sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        while (sInQueue) {
            new UDPConnection().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "Update");

            try {
                sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}