package dturic.faculty.person.hr.krizickruzic.http;

import android.content.Context;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.util.ArrayList;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.http.models.Friend;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;
import dturic.faculty.person.hr.krizickruzic.http.models.Response;
import dturic.faculty.person.hr.krizickruzic.http.models.ResponseFriends;
import dturic.faculty.person.hr.krizickruzic.http.models.ResponseLogin;
import dturic.faculty.person.hr.krizickruzic.http.models.StatisticsDataResponse;
import dturic.faculty.person.hr.krizickruzic.http.models.StatisticsResonse;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class DataProvider implements RetrofitProvider {

    public interface Provider {

        @POST("/login")
        @FormUrlEncoded
        Call<ResponseLogin> login(@Field("username") String userName,
                                  @Field("password") String password);

        @POST("/register")
        @FormUrlEncoded
        Call<ResponseLogin> register(@Field("username") String userName,
                                     @Field("password") String password,
                                     @Field("email") String email);

        @POST("/resetPassword")
        @FormUrlEncoded
        Call<ResponseLogin> resetPassword(@Field("password") String password,
                                          @Field("repeated_password") String repeatedPassword,
                                          @Field("email") String email,
                                          @Field("new_password") String newPassword);

        @POST("/forgottenPassword")
        @FormUrlEncoded
        Call<ResponseLogin> forgottenPassword(@Field("username") String username,
                                              @Field("email") String email);

        @POST("/getInQuery")
        @FormUrlEncoded
        Call<String> getInQuery(@Field("user_id") String userId);

        @POST("/getFriendList")
        @FormUrlEncoded
        Call<ResponseFriends> getFriendList(@Field("user_id") String userId);

        @POST("/getUserById")
        @FormUrlEncoded
        Call<ResponseLogin> getUserById(@Field("user_id") String userId);

        @POST("/getServerAddress")
        Call<String> getServerAddress();

        @POST("/removePlayerFromFriendQueue")
        @FormUrlEncoded
        void removePlayerFromFriendQueue(@Field("user_id") String userId);

        @POST("/sendFriendRequest")
        @FormUrlEncoded
        Call<String> sendFriendRequest(@Field("user_id") String userId, @Field("username") String username, @Field("friend_username") String friendUsername);

        @POST("/getFriendRequests")
        @FormUrlEncoded
        Call<ResponseFriends> getFriendRequests(@Field("user_id") String userId);

        @POST("/acceptFriendRequest")
        @FormUrlEncoded
        Call<Response> acceptFriendRequest(@Field("user_id") String userId, @Field("friend_id") String friendId);

        @POST("/declineFriendRequest")
        @FormUrlEncoded
        Call<Response> declineFriendRequest(@Field("user_id") String userId, @Field("friend_id") String friendId);

        @POST("/getStatistics")
        @FormUrlEncoded
        Call<StatisticsDataResponse> getStatistics(@Field("user_id") String userId);
    }

    static private DataProvider sInstance;
    static private Provider sProvider;

    private LoginListener mLoginListener;
    private RegisterListener mRegisterListener;
    private QueueListener mQueueListener;
    private ServerAddressListener mAddressListener;
    private FriendListListener mFriendListListener;
    private FriendRequestListener mFriendRequestListener;
    private StatisticsListener mStatisticsListener;

    public static DataProvider getInstance() {
        if (sInstance == null) {
            sInstance = new DataProvider();
        }

        return sInstance;
    }

    public void create(Context context) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        clientBuilder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder().header("auth", "K7DT8M18PLOM").build();

                return chain.proceed(request);
            }
        });

        OkHttpClient client = clientBuilder.build();

        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("http://192.168.1.7")
                .baseUrl(context.getString(R.string.address))
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        sProvider = retrofit.create(Provider.class);
    }

    @Override
    public void login(String username, String password) {
        Call<ResponseLogin> response = sProvider.login(username, password);

        response.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(@NonNull Call<ResponseLogin> call, @NonNull retrofit2.Response<ResponseLogin> response) {
                if (response.code() > 399) {
                    mLoginListener.onFailure("Internal error: code" + response.code());
                    return;
                }
                ResponseLogin data = response.body();
                if (data == null) {
                    mLoginListener.onFailure("Empty response");
                    return;
                }
                if (data.getData() != null) {
                    mLoginListener.onSuccess(data.getData());
                } else if (data.getMessage() != null) {
                    mLoginListener.onFailure(data.getMessage());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseLogin> call, @NonNull Throwable t) {
                mLoginListener.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void register(String username, String email, String password) {

        Call<ResponseLogin> response = sProvider.register(username, password, email);

        response.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(@NonNull Call<ResponseLogin> call, @NonNull retrofit2.Response<ResponseLogin> response) {
                if (response.code() > 299) {
                    mRegisterListener.onFailure("Internal error");
                    return;
                }

                ResponseLogin data = response.body();
                if (data == null) {
                    mRegisterListener.onFailure("Empty response");
                    return;
                }
                if (data.getData() != null) {
                    mRegisterListener.onSuccess(data.getData());
                } else if (data.getMessage() != null) {
                    mRegisterListener.onFailure(data.getMessage());
                }

            }

            @Override
            public void onFailure(@NonNull Call<ResponseLogin> call, @NonNull Throwable t) {
                mRegisterListener.onFailure("Internal error");
            }
        });
    }

    @Override
    public void forgottenPassword(String username, String email) {

        Call<ResponseLogin> response = sProvider.forgottenPassword(username, email);

        response.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(@NonNull Call<ResponseLogin> call, @NonNull retrofit2.Response<ResponseLogin> response) {
                if (response.code() > 299) {
                    mLoginListener.onFailure("Internal error");
                    return;
                }
                ResponseLogin data = response.body();

                if (data == null) {
                    mLoginListener.onFailure("Empty response");
                } else if (data.getMessage() != null) {
                    mLoginListener.onFailure(data.getMessage());
                } else {
                    mLoginListener.onSuccess(data.getData());
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseLogin> call, @NonNull Throwable t) {
                mLoginListener.onFailure("Internal error");
            }
        });
    }

    @Override
    public void getInQuery(String userId) {
        Call<String> response = sProvider.getInQuery(userId);

        response.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response) {
                mQueueListener.onSuccessQueue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                String message = t.getMessage();
                mQueueListener.onFailureQueue(message);
            }
        });
    }

    @Override
    public void resetPassword(String password, String repeatedPassword, String email, String newPassword) {
        Call<ResponseLogin> response = sProvider.resetPassword(password, repeatedPassword, email, newPassword);

        response.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(@NonNull Call<ResponseLogin> call, @NonNull retrofit2.Response<ResponseLogin> response) {
                if (response.code() > 299) {
                    return;
                }
                ResponseLogin data = response.body();
                if (data == null) {
                    return;
                } else if (data.getMessage() != null) {

                } else if (data.getData() != null) {
                    data.getData();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseLogin> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void getFriendList(String userId) {
        Call<ResponseFriends> response = sProvider.getFriendList(userId);

        response.enqueue(new Callback<ResponseFriends>() {
            @Override
            public void onResponse(@NonNull Call<ResponseFriends> call, @NonNull retrofit2.Response<ResponseFriends> response) {
                if (response.code() > 299) {
                    return;
                }

                ResponseFriends data = response.body();

                if (data == null) {
                    mFriendListListener.onFailure("");
                } else if (!data.getMessage().equals("")) {
                    mFriendListListener.onFailure(data.getMessage());
                } else {
                    ArrayList<Friend> friendsList = data.getData();
                    mFriendListListener.onSucces(friendsList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseFriends> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void getUserById(String userId) {
        Call<ResponseLogin> response = sProvider.getUserById(userId);

        response.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(@NonNull Call<ResponseLogin> call, @NonNull retrofit2.Response<ResponseLogin> response) {
                if (response.code() > 299) {
                    return;
                }

                ResponseLogin data = response.body();
                if (data == null) {

                } else if (data.getMessage() != null) {

                } else if (data.getData() != null) {
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseLogin> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void getServerAddress() {
        Call<String> response = sProvider.getServerAddress();

        response.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response) {
                mAddressListener.onSuccess(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                String message = t.getMessage();
                mAddressListener.onFailure(message);
            }
        });
    }

    @Override
    public void sendFriendRequest(String userId, String username, String friendUsername) {
        Call<String> reponse = sProvider.sendFriendRequest(userId, username, friendUsername);

        reponse.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull retrofit2.Response<String> response) {

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void getFriendRequests(String userId) {
        Call<ResponseFriends> response = sProvider.getFriendRequests(userId);

        response.enqueue(new Callback<ResponseFriends>() {
            @Override
            public void onResponse(@NonNull Call<ResponseFriends> call, @NonNull retrofit2.Response<ResponseFriends> response) {
                if (response.code() > 299) {
                    return;
                }

                ResponseFriends data = response.body();

                if (data == null) {
                    mFriendListListener.onFailure("");
                } else if (!data.getMessage().equals("")) {
                    mFriendListListener.onFailure(data.getMessage());
                } else {
                    ArrayList<Friend> friendsList = data.getData();
                    mFriendListListener.onSucces(friendsList);
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseFriends> call, @NonNull Throwable t) {

            }
        });
    }

    @Override
    public void getStatistics(String userId) {
        Call<StatisticsDataResponse> response = sProvider.getStatistics(userId);

        response.enqueue(new Callback<StatisticsDataResponse>() {
            @Override
            public void onResponse(@NonNull Call<StatisticsDataResponse> call, @NonNull retrofit2.Response<StatisticsDataResponse> response) {

                StatisticsDataResponse data = response.body();

                if (data == null) {
                    mStatisticsListener.onFailure("Empty response");
                } else if (data.getData() != null) {
                    mStatisticsListener.onSuccess((StatisticsResonse) data.getData());
                } else if (data.getMessage() != null) {
                    mStatisticsListener.onFailure(data.getMessage());
                }
            }

            @Override
            public void onFailure(Call<StatisticsDataResponse> call, Throwable t) {
                mStatisticsListener.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void acceptFriendRequest(String userId, String friendId) {
        Call<Response> response = sProvider.acceptFriendRequest(userId, friendId);

        response.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                Response data = response.body();

                if (data == null) {
                    mFriendRequestListener.onFailure(response.message());
                } else if (!data.getMessage().equals("Success")) {
                    mFriendRequestListener.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                mFriendRequestListener.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void declineFriendRequest(String userId, String friendId) {
        Call<Response> response = sProvider.declineFriendRequest(userId, friendId);

        response.enqueue(new Callback<Response>() {
            @Override
            public void onResponse(Call<Response> call, retrofit2.Response<Response> response) {

                Response data = response.body();

                if (data == null) {
                    mFriendRequestListener.onFailure(response.message());
                } else if (!data.getMessage().equals("Success")) {
                    mFriendRequestListener.onFailure(response.message());
                }
            }

            @Override
            public void onFailure(Call<Response> call, Throwable t) {
                mFriendRequestListener.onFailure(t.getMessage());
            }
        });
    }

    @Override
    public void removePlayerFromFriendQueue(String userId) {
        //sProvider.removePlayerFromFriendQueue(userId);
    }

    public void setLoginListener(LoginListener listener) {
        mLoginListener = listener;
    }

    public void setRegisterLIstener(RegisterListener lIstener) {
        mRegisterListener = lIstener;
    }

    public void setQueueListener(QueueListener lIstener) {
        mQueueListener = lIstener;
    }

    public void setAddressListener(ServerAddressListener listener) {
        mAddressListener = listener;
    }

    public void setFriendListListener(FriendListListener listener) {
        mFriendListListener = listener;
    }

    public void setFriendRequestListener(FriendRequestListener listener) {
        mFriendRequestListener = listener;
    }

    public void setStatisticsListener(StatisticsListener listener) {
        mStatisticsListener = listener;
    }

    public interface LoginListener {
        void onSuccess(LoginData data);

        void onFailure(String message);
    }

    public interface RegisterListener {
        void onSuccess(LoginData data);

        void onFailure(String message);
    }

    public interface QueueListener {
        void onSuccessQueue(String address);

        void onFailureQueue(String message);
    }

    public interface ServerAddressListener {
        void onSuccess(String address);

        void onFailure(String message);
    }

    public interface FriendListListener {
        void onSucces(ArrayList<Friend> friends);

        void onFailure(String message);
    }

    public interface FriendRequestListener {

        void onFailure(String message);
    }

    public interface StatisticsListener {

        void onSuccess(StatisticsResonse data);

        void onFailure(String message);
    }
}