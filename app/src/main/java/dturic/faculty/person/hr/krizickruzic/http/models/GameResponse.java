package dturic.faculty.person.hr.krizickruzic.http.models;

import com.google.gson.annotations.SerializedName;

public class GameResponse {
    @SerializedName("game_id")
    private String gameId;
    @SerializedName("player_one")
    private String playerOne;
    @SerializedName("player_two")
    private String playerTwo;
    @SerializedName("time")
    private long time;
    @SerializedName("winner")
    private String winner;
    @SerializedName("draw")
    private boolean draw;
    @SerializedName("turns")
    private int turns;


    public String getGameId() {
        return gameId;
    }

    public String getPlayerOne() {
        return playerOne;
    }

    public String getPlayerTwo() {
        return playerTwo;
    }

    public long getTime() {
        return time;
    }

    public String getWinner() {
        return winner;
    }

    public boolean isDraw() {
        return draw;
    }

    public int getTurns() {
        return turns;
    }
}
