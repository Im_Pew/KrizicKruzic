package dturic.faculty.person.hr.krizickruzic.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.fragment.dialog.LoginDialogFragment;
import dturic.faculty.person.hr.krizickruzic.fragment.dialog.RegisterDialogFragment;

public class MainScreenFragment extends Fragment implements View.OnClickListener {

    public MainScreenFragment() {
        // Required empty public constructor
    }

    public static MainScreenFragment newInstance() {
        return new MainScreenFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_main_screen, container, false);

        view.findViewById(R.id.login).setOnClickListener(this);
        view.findViewById(R.id.register).setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.login:
                LoginDialogFragment dialogFragmentLogin = LoginDialogFragment.newInstance();
                dialogFragmentLogin.show(getChildFragmentManager(), "login_dialog_fragment");
                break;
            case R.id.register:
                RegisterDialogFragment dialogFragmentRegister = RegisterDialogFragment.newInstance();
                dialogFragmentRegister.show(getChildFragmentManager(), "register_dialog_fragment");
                break;
        }
    }
}
