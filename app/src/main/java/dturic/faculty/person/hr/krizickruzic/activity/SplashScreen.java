package dturic.faculty.person.hr.krizickruzic.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;

public class SplashScreen extends AppCompatActivity implements DataProvider.ServerAddressListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(CONNECTIVITY_SERVICE);
        if (connectivityManager == null) {
            return;
        }

        if (connectivityManager.getActiveNetworkInfo().isConnectedOrConnecting()) {
            DataProvider instance = DataProvider.getInstance();
            instance.create(this);
            instance.setAddressListener(this);
            instance.getServerAddress();
        }
    }

    @Override
    public void onSuccess(String address) {
        UserUtil.saveServerAddress(this, address);
        startActivity(new Intent(this, MainActivity.class));

    }

    @Override
    public void onFailure(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
