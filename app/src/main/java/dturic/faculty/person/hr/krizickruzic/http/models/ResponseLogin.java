package dturic.faculty.person.hr.krizickruzic.http.models;

import com.google.gson.annotations.SerializedName;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class ResponseLogin {

    @SerializedName("code")
    private String code;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private LoginData data;

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public LoginData getData() {
        return data;
    }
}
