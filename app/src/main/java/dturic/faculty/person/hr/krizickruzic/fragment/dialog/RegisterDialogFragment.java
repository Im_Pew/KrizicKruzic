package dturic.faculty.person.hr.krizickruzic.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.fragment.MainScreenLoggedFragment;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class RegisterDialogFragment extends DialogFragment implements View.OnClickListener, DataProvider.RegisterListener {

    private EditText mUsername;
    private EditText mEmail;
    private EditText mPassword;
    private EditText mRepeatedPassword;
    private Button mRegister;

    public static RegisterDialogFragment newInstance() {
        return new RegisterDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_register_dialog, container);

        mUsername = view.findViewById(R.id.username);
        mEmail = view.findViewById(R.id.email);
        mPassword = view.findViewById(R.id.password);
        mRepeatedPassword = view.findViewById(R.id.repeated_password);
        mRegister = view.findViewById(R.id.register);
        mRegister.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.register:

                if (mPassword.getText().toString().equals(mRepeatedPassword.getText().toString())) {
                    DataProvider.getInstance().register(mUsername.getText().toString(), mEmail.getText().toString(), mPassword.getText().toString());
                }
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        DataProvider.getInstance().setRegisterLIstener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        DataProvider.getInstance().setRegisterLIstener(null);
    }

    @Override
    public void onSuccess(LoginData data) {
        mRegister.setText(data.getUserId());

        UserUtil.saveUser(getContext(), data);
        getActivity().getSupportFragmentManager().beginTransaction().replace(android.R.id.content, MainScreenLoggedFragment.newInstance()).commit();
    }

    @Override
    public void onFailure(String message) {
        mRegister.setText(message);
    }
}
