package dturic.faculty.person.hr.krizickruzic.udp;

import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

import dturic.faculty.person.hr.krizickruzic.udp.model.GameData;

public class GameListener extends AsyncTask<Void, Void, GameData> {

    private static DatagramSocket socket;
    private static OnGameReady OnGameReady;

    public static void setListener(OnGameReady g) {
        OnGameReady = g;
    }
    //TODO OnGameReady load indicator

    GameListener(DatagramSocket datagramSocket) {
        socket = datagramSocket;
    }

    @Override
    protected GameData doInBackground(Void... voids) {

        DatagramPacket packetRecv;
        GameData gameData = null;
        String receivedDataString = "";
        byte[] receivedData = new byte[1024];
        try {
            packetRecv = new DatagramPacket(receivedData, receivedData.length);

            socket.receive(packetRecv);

            receivedDataString = new String(receivedData, 0, packetRecv.getLength());

            Gson gson = new Gson();
            gameData = gson.fromJson(receivedDataString, GameData.class);
            while (TextUtils.isEmpty(gameData.getGameId())) {
                socket.receive(packetRecv);
                gameData = gson.fromJson(receivedDataString, GameData.class);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gameData;
    }

    @Override
    protected void onPostExecute(GameData game) {
        super.onPostExecute(game);
        ConnectionUpdater.setInQueue(false);

        GameListener.OnGameReady.onFound(game);

//        GameUDPConnection.setSocket(socket);
        socket.close();

        UDPConnection.removeGameListener();
        UDPConnection.removeUpdater();

        Log.i("Game info:", "OnGameReady started");
        this.cancel(true);
    }

    public interface OnGameReady {
        void onFound(GameData game);
    }
}
