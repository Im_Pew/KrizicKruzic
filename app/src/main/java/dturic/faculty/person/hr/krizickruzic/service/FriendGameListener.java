package dturic.faculty.person.hr.krizickruzic.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;
import dturic.faculty.person.hr.krizickruzic.udp.model.FriendGameRequest;

public class FriendGameListener extends Service {
    public FriendGameListener() {
    }

    private Handler handler;
    private LoginData user;
    private FriendGameListenerAsynctask mListenerAsyntask;
    @SuppressLint("StaticFieldLeak")
    private static InetAddress mAddress;
    private static OnRequest sListener;

    public static void setOnRequestListener(OnRequest sListener) {
        FriendGameListener.sListener = sListener;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }

    @Override
    public void onCreate() {
        handler = new Handler();
        super.onCreate();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.


        Log.i("Friend service", "started");

        user = UserUtil.getUser(this);

        String address = UserUtil.getServerAddress(this);

        try {
            mAddress = InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        mListenerAsyntask = new FriendGameListenerAsynctask(this, user);
        mListenerAsyntask.start();

        return START_NOT_STICKY;
    }

    private void runOnUiThread(Runnable runnable) {
        handler.post(runnable);
    }

    @Override
    public void onDestroy() {

        DataProvider.getInstance().removePlayerFromFriendQueue(user.getUserId());
        super.onDestroy();
    }

    public class FriendGameListenerAsynctask extends Thread {

        private LoginData mUser;
        private Context mContext;

        public FriendGameListenerAsynctask(Context context, LoginData user) {
            this.mContext = context;
            this.mUser = user;
        }

        private DatagramSocket socket;

        @Override
        public void run() {
            super.run();

            DatagramPacket packet;
            try {
                socket = new DatagramSocket();
            } catch (SocketException e) {
                e.printStackTrace();
            }

            FriendGameRequest response = null;

            Gson gson = new Gson();
            FriendGameRequest request = new FriendGameRequest();
            request.setUserId(mUser.getUserId());
            request.setStatus("Register");
            String jsonData = gson.toJson(request);

            byte[] receivedData = new byte[1024];
            try {
                packet = new DatagramPacket(jsonData.getBytes(), jsonData.length(), mAddress, 1500);

                socket.send(packet);

                packet = new DatagramPacket(receivedData, receivedData.length);

                socket.receive(packet);

                response = gson.fromJson(new String(receivedData, 0, packet.getLength()), FriendGameRequest.class);

            } catch (Exception e) {
                e.printStackTrace();
            }

            final FriendGameRequest finalResponse = response;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    FriendGameListener.sListener.onRequest(finalResponse);
                }
            });
        }
    }

    public interface OnRequest {
        void onRequest(FriendGameRequest response);
    }
}
