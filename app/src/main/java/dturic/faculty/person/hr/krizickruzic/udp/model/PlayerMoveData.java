package dturic.faculty.person.hr.krizickruzic.udp.model;

import com.google.gson.annotations.SerializedName;

/**
 * @Author Danijel Turić
 * 2018
 * KrizicKruzic
 */
public class PlayerMoveData {
    @SerializedName("game_id")
    private String gameId;
    @SerializedName("user_id")
    private String userId;
    @SerializedName("turn")
    private int turn;
    @SerializedName("field")
    private int field;
    @SerializedName("player")
    private int player;
    @SerializedName("winner")
    private int winner;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getTurn() {
        return turn;
    }

    public void setTurn(int turn) {
        this.turn = turn;
    }

    public int getField() {
        return field;
    }

    public void setField(int field) {
        this.field = field;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }

    public int getWinner() {
        return winner;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}
