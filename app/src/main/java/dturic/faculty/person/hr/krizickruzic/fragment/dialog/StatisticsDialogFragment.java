package dturic.faculty.person.hr.krizickruzic.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import dturic.faculty.person.hr.krizickruzic.R;
import dturic.faculty.person.hr.krizickruzic.UserUtil;
import dturic.faculty.person.hr.krizickruzic.adapter.HistoryGamesAdapter;
import dturic.faculty.person.hr.krizickruzic.http.DataProvider;
import dturic.faculty.person.hr.krizickruzic.http.models.LoginData;
import dturic.faculty.person.hr.krizickruzic.http.models.StatisticsResonse;

public class StatisticsDialogFragment extends DialogFragment implements DataProvider.StatisticsListener {

    TextView mWins;
    TextView mDraws;
    TextView mLosses;
    TextView mWinsTittle;
    TextView mDrawsTittle;
    TextView mLossesTittle;

    HistoryGamesAdapter mAdapter;
    RecyclerView mGamesHistory;

    public static StatisticsDialogFragment newInstance() {
        return new StatisticsDialogFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DataProvider.getInstance().setStatisticsListener(this);
        LoginData user = UserUtil.getUser(getContext());
        DataProvider.getInstance().getStatistics(user.getUserId());

        mAdapter = new HistoryGamesAdapter();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(R.layout.fragment_statistics_dialog, container, false);

        mWins = view.findViewById(R.id.wins);
        mWinsTittle = view.findViewById(R.id.wins_tittle);
        mDraws = view.findViewById(R.id.draws);
        mDrawsTittle = view.findViewById(R.id.draws_tittle);
        mLosses = view.findViewById(R.id.losses);
        mLossesTittle = view.findViewById(R.id.losses_tittle);

        mGamesHistory = view.findViewById(R.id.games_history);

        mGamesHistory.setAdapter(mAdapter);
        mGamesHistory.setLayoutManager(new LinearLayoutManager(getContext()));
        setText();

        return view;
    }

    private void setText() {
        mWinsTittle.setText("Pobjede");
        mDrawsTittle.setText("Neriješeno");
        mLossesTittle.setText("Porazi");
    }

    @Override
    public void onSuccess(StatisticsResonse data) {
        mWins.setText(String.valueOf(data.getWins()));
        mDraws.setText(String.valueOf(data.getDraws()));
        mLosses.setText(String.valueOf(data.getLoses()));

        mAdapter.setData(data.getLastFiveGames());
    }

    @Override
    public void onFailure(String message) {

    }
}
