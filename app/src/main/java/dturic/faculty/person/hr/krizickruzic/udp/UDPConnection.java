package dturic.faculty.person.hr.krizickruzic.udp;


import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import dturic.faculty.person.hr.krizickruzic.udp.model.GameRequest;

public class UDPConnection extends AsyncTask<String, String, String> {

    private static GameListener mListener = null;
    private static ConnectionUpdater mUpdater = null;

    private static DatagramSocket socket;
    private static DatagramPacket packet;

    public static void removeGameListener() {
        mListener = null;
    }

    public static void removeUpdater() {
        mUpdater = null;
    }

    static private byte[] sByteAddress;
    static private String sUserId;

    public static void setServerAddress(byte[] addr) {
        sByteAddress = addr;
    }

    public static void setsUserId(String userId) {
        sUserId = userId;
    }

    @Override
    protected String doInBackground(String... voids) {

        String modifiedSentence = "";
        byte[] recivedData = new byte[1024];

        try {
            InetAddress address = InetAddress.getByAddress(sByteAddress);

            Log.i("Address:", address.toString());

            if (socket == null || socket.isClosed()) {
                socket = new DatagramSocket();
            }

            Gson gson = new Gson();

            GameRequest request = new GameRequest(sUserId, voids[0]);
            String json = gson.toJson(request);

            packet = new DatagramPacket(json.getBytes(), json.length(), address, 1000);
            socket.send(packet);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return modifiedSentence;
    }

    @Override
    protected void onPostExecute(String response) {

        if (mUpdater == null) {
            ConnectionUpdater.setInQueue(true);
            mUpdater = new ConnectionUpdater();
            mUpdater.start();
        }

        if (mListener == null) {
            mListener = new GameListener(socket);
            mListener.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }

        super.onPostExecute(response);
    }
}